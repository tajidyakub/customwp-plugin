/**
 * Grunt tasks definition file.
 *
 * Adjust configurations in the given config file
 * and rename accodingly tobe required from this file.
 */
const config = require("./_config/config");

module.exports = function(grunt) {
  grunt.initConfig({
    // Deployment through rsync, will synchronize local dev folder with
    // Web root, either local or remote
    rsync: {
      options: {
        args: ["--verbose"],
        exclude: config.exclude,
        recursive: true
      },
      remote: {
        options: {
          src: config.localPath,
          dest: config.remotePath,
          delete: true
        }
      }
    },
    compress: {
      plugins: {
        options: {
          archive: "released/" + config.slug + "-" + config.version + ".zip"
        },
        expand: true,
        cwd: "src/plugins/",
        src: ["**/*"],
        dest: "/"
      }
    }
  });

  // Next one would load plugins
  grunt.loadNpmTasks("grunt-rsync");
  grunt.loadNpmTasks("grunt-contrib-compress");
  grunt.registerTask("deploy", "Deploy to remote web server", ["rsync:remote"]);
  grunt.registerTask("release", "Compress files in dist/", ["compress:plugins"]);
};
