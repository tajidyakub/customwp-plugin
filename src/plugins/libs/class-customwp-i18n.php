<?php
class CustomWPI18n {

    public function load_plugin_textdomain() {
        load_plugin_textdomain(
            'customwp',
            false,
            dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
        );
    }

}
