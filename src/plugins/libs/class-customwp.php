<?php
/**
 * Core class for the plugin
 */

class CustomWP {
    protected $slug;
    protected $version;
    protected $loader;

    public function __construct() {
        // Populate properties
        $this->slug     = CUSTOMWP_SLUG;
        $this->version  = CUSTOMWP_VERSION;
        // Load Dependencies
        $this->loaddeps();
        $this->set_locale();
        $this->set_shortcodes();
        $this->load_assets();
    }

    private function loaddeps() {
        require_once plugin_dir_path(dirname(__FILE__)) . 'libs/class-customwp-i18n.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'libs/class-customwp-shortcodes.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'libs/class-customwp-utils.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'libs/class-customwp-loader.php';
        $this->loader = new CustomWPLoader();
    }

    private function set_locale() {
        $i18n = new CustomWPI18n();
        $this->loader->add_action( 'plugins_loaded', $i18n, 'load_plugin_textdomain' );
    }

    private function set_shortcodes() {
        $shortcodes = new CustomWPShortcodes();
        $this->loader->add_action('init', $shortcodes, 'init_shortcodes');
    }

    private function load_assets() {
        $utils = new CustomWPUtils();
        $this->loader->add_action('wp_enqueue_scripts', $utils, 'enqueues');
    }

    public function run() {
        $this->loader->run();
    }

    public function get_plugin_name() {
        return $this->plugin_name;
    }

    public function get_loader() {
        return $this->loader;
    }

    public function get_version() {
        return $this->version;
    }
}
