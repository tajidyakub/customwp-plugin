<?php
/**
 * Classes for shortcodes.
 */

class CustomWPShortcodes {

    public function __construct() {
        //
    }

    public function init_shortcodes() {
        add_shortcode('alert', $this, 'alert_shortcode_cb');
    }

    private function alert_shortcode_cb( $atts , $content = null ) {
        $atts = shortcode_atts(
            array(
                'type' => 'info',
            ),
            $atts,
            'alert'
        );
        return '<p class="customwp-alert customwp-alert-'.$atts['type'].'">'.do_shortcode($content).'</p>';
    }
}
