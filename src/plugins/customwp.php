<?php
/**
 * Plugin's main entry file.
 *
 * @package customwp
 * @version 1.0.0
 * @link    https://bitbucket.org/tajidyakub/customwp-plugin
 *
 * @customwp
 * Plugin Name:       Customwp
 * Plugin URI:        https://bitbucket.org/tajidyakub/customwp-plugin
 * Description:       WordPress Plugin to customize your WordPress - without the GUI.
 * Version:           1.0.0
 * Author:            Tajid Yakub
 * Author URI:        https://tajidyakub.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       customwp
 * Domain Path:       /languages
 */

if ( ! defined( 'WPINC' ) ) {
    die;
}

define('CUSTOMWP_SLUG', 'customwp');
define('CUSTOMWP_VERSION', '1.0.0');

// require init class
require_once plugin_dir_path( __FILE__ ) . 'libs/class-customwp-init.php';

function customwp_activate() {
    CustomWPInit::activate();
}

function customwp_deactivate() {
    CustomWPInit::deactivate();
}
// activation and deactivateion hook register
register_activation_hook( __FILE__, 'customwp_activate' );
register_deactivation_hook( __FILE__, 'customwp_deactivate' );

// require core class
require_once plugin_dir_path( __FILE__ ) . 'libs/class-customwp.php';

// require core class
function customwp_exec() {
    $customwp = new CustomWP();
    $customwp->run();
}

customwp_exec();
